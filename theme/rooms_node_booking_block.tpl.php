<?php
/**
 * @file
 * Template file for book now button.
 */
?>

<div class="booking-block">
  <div class="price"><?php print drupal_render($items['price']); ?></div>
  <div class="booking-buttons"><?php print drupal_render($items['button']); ?></div>
  <div class="date_range"><?php print drupal_render($items['date_range']); ?></div>
</div>
