<?php
/**
 * @file
 * Views handler filter date for rooms node.
 */

/**
 * All the comparisons need to be done with SQL DATETIMES and not unixtime.
 */
class rooms_node_handler_filter_datetime extends views_handler_filter_date {

  /**
   * Convert an input date.
   *
   * We use strtotime() to accept a wide range of date inputs and then
   * convert the unixtime back to SQL DATETIME before adding the WHERE clause.
   */
  function op_simple($field) {
    $value = intval(strtotime($this->value['value'], 0));

    if (!empty($this->value['type']) && $this->value['type'] == 'offset') {
      // Keep sign.
      $value = time() + sprintf('%+d', $value);
    }

    $value = $this->format_date($value);

    $this->query->add_where($this->options['group'], $field, $value, $this->operator);
  }

  /**
   * Search available on interval.
   *
   * Implement booking search excluding results that have a date booked in
   * min max interval.
   */
  function op_between($field) {
    if ($this->operator == 'between') {
      $a = intval(strtotime($this->value['min'], 0));
      $b = intval(strtotime($this->value['max'], 0));
    }
    else {
      $a = intval(strtotime($this->value['max'], 0));
      $b = intval(strtotime($this->value['min'], 0));

      $this->query->set_where_group('OR', $this->options['group']);
    }

    if ($this->value['type'] == 'offset') {
      $now = time();
      // Keep sign.
      $a = $now + sprintf('%+d', $a);
      // Keep sign.
      $b = $now + sprintf('%+d', $b);
    }

    $a = $this->format_date($a);
    $b = $this->format_date($b);

    $queryz = db_select('rooms_availability_index', 'r')
      ->fields('r', array('nid'))
      ->condition('r.date', array($a), '>=')
      ->condition('r.date', array($b), '<')
      ->condition('r.state', '0', '=')
      ->groupBy('r.nid');

    $result_ok = $queryz->execute();

    $nids = array();

    foreach ($result_ok as $unit) {
      $nids[] = $unit->nid;
    }

    if ($a > 0 && $b > 0) {
      $this->query->add_where($this->options['group'], 'rooms_availability_index.date', $a, '>=');
      $this->query->add_where($this->options['group'], 'rooms_availability_index.date', $b, '<');
      if (count($nids)) {
        $this->query->add_where($this->options['group'], 'node.nid', $nids, 'NOT IN');
      }
    }
  }

  /**
   * Format filter date.
   */
  function format_date($unixtime) {
    return date("Y-m-d H:i:s", $unixtime);
  }

}
