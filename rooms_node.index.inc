<?php
/**
 * @file
 * Some useful function to regenerate the node availability and prices.
 */

/**
 * Rooms node create an index of all dates.
 *
 * Index are used by views module for search availability of nodes
 * and their prices in a date interval.
 * Each time an availability or a price are updated, or a customer booking a
 * resource the index must be updated.
 *
 * @param int $unit_id
 *   Rooms unit id.
 */
function rooms_node_update_index($unit_id = 0) {

  if (is_object($unit_id)) {
    return FALSE;
  }

  if (!$unit_id) {
    return FALSE;
  }

  // Convert date format.
  $today = date('Y-m-d 00:00:00', time());
  $today_timestamp = strtotime($today);

  $start_date = date_create()->setTimestamp($today_timestamp);
  $end_date = date_create()->setTimestamp(strtotime('+1 year', $today_timestamp));

  $period = new DatePeriod(
    $start_date,
    new DateInterval('P1D'),
    $end_date
  );

  $room = entity_load('rooms_unit', array($unit_id));
  $default_price = $room[$unit_id]->base_price;
  $default_availability = $room[$unit_id]->default_state;
  $nid = rooms_node_get_nid_by_unit_id($unit_id);

  // Select from the table of the module rooms all for availability.
  $query_availability = "SELECT * FROM {rooms_availability} WHERE unit_id = :unit_id";

  // Select from the table of the form rooms all values  inseiriti for prices.
  $query_price = "SELECT * FROM {rooms_pricing} WHERE unit_id = :unit_id";

  $result = db_query($query_availability, array(':unit_id' => $unit_id));
  $result_price = db_query($query_price, array(':unit_id' => $unit_id));

  // Load default values for x days.
  $all_dates = array();
  $unit_infos = array();

  foreach ($period as $single) {
    $all_dates[$single->getTimestamp()] = $single->getTimestamp();
    $unit_infos[$single->getTimestamp()][':unit_id'] = $unit_id;
    $unit_infos[$single->getTimestamp()][':state'] = $default_availability;
    $unit_infos[$single->getTimestamp()][':date'] = date('Y-m-d 00:00:00', $single->getTimestamp());
    $unit_infos[$single->getTimestamp()][':price'] = $default_price;
    $unit_infos[$single->getTimestamp()][':nid'] = $nid;
  }

  /*
   * Now you have to save the index (tab: rooms_availability_index)
   * values date / price / available
   * - Row added for each day/apartment that stores date/price/available.
   * - If not defined a specific price for a given we'll use the default price.
   * - If it is not defined for a specific availability date availability
   *   you will use the default.
   * - Will be included all dates, from the date
   * Current now () up to 365 days for each resource bookabile.
   */

  foreach ($result as $month) {
    for ($i = 1; $i <= 31; $i++) {
      if (checkdate($month->month, $i, $month->year)) {
        $day = 'd' . $i;
        $date = $month->year . "-" . $month->month . "-" . $i . " 00:00:00";
        $date_time = new DateTime($date);
        $rooms_date = $date_time->getTimestamp();
        $unit_infos[$rooms_date][':unit_id'] = $unit_id;
        $unit_infos[$rooms_date][':state'] = $month->$day;
        $unit_infos[$rooms_date][':date'] = date('Y-m-d 00:00:00', $rooms_date);
        $unit_infos[$rooms_date][':price'] = $default_price;
        $unit_infos[$rooms_date][':nid'] = $nid;
      }
    }
  }

  /*
   * This is the specific query to retrieve the info on the
   * prices of individual dates.
   */

  foreach ($result_price as $month) {
    for ($i = 1; $i <= 31; $i++) {
      if (checkdate($month->month, $i, $month->year)) {
        $day = 'd' . $i;
        $date = $month->year . "-" . $month->month . "-" . $i . " 00:00:00";
        $date_time = new DateTime($date);
        $rooms_date = $date_time->getTimestamp();
        $unit_infos[$rooms_date][':price'] = $month->$day / 100;
      }
    }
  }

  /*
   * Before updating all the values empty the old data of this unit_id
   * table structure : unit_id | date | state
   */
  db_delete('rooms_availability_index')->condition('unit_id', $unit_id)->execute();

  // Insert in the index all the updated values in $unit_infos.
  if (is_array($unit_infos)) {
    foreach ($all_dates as $std_date) {
      db_insert('rooms_availability_index')
        ->fields(array(
         'unit_id' => $unit_infos[$std_date][':unit_id'],
         'state' => $unit_infos[$std_date][':state'],
         'date' => $unit_infos[$std_date][':date'],
         'price' => $unit_infos[$std_date][':price'],
         'nid' => $unit_infos[$std_date][':nid'],
      ))->execute();
    }
  }

}
