<?php
/**
 * @file
 * Admin function to add some rooms_node options in node settings form.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function rooms_node_form_node_type_form_alter(&$form, &$form_state, $form_id) {
  // Define field set in additional settings group.
  $form['rooms_node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Rooms Node'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 10,
    '#group' => 'additional_settings',
  );

  $form['rooms_node']['enable_rooms_node'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable rooms node for this content type'),
  );

  $unit_type = rooms_node_get_unit_types();

  $unit_type_required = variable_get('rooms_node_' . $form['#node_type']->type . "_unit_type") == 1 ? TRUE : FALSE;
  // Define field set in additional settings group.
  $form['rooms_node']['node_unit_type'] = array(
    '#type' => 'select',
    '#required' => $unit_type_required,
    '#options' => $unit_type,
    '#title' => t('Rooms Type for this content type'),
  );

  // Set the number of days that cron must regenerate i the index.
  $form['rooms_node']['days_in_index_index'] = array(
    '#type' => 'textfield',
    '#title' => t('How many days for each node rooms_node saving in the index (es:365)'),
  );

  // Set default value from automatically saved config variable.
  $form['rooms_node']['days_in_index_index']['#default_value'] = variable_get('rooms_node_days_in_index_' . $form['#node_type']->type, 365);
  // Set default value from automatically saved config variable.
  $enabled = variable_get('rooms_node_' . $form['#node_type']->type, 0);
  $form['rooms_node']['enable_rooms_node']['#default_value'] = $enabled ? 1 : 0;

  $form['rooms_node']['node_unit_type']['#default_value'] = variable_get('rooms_node_' . $form['#node_type']->type . "_unit_type");

  $form['#submit'][] = 'rooms_node_node_type_save_submit';
}


/**
 * Return all room_units types.
 */
function rooms_node_get_unit_types() {
  $all_unit_types = rooms_unit_get_types();
  $unit_type = array();
  foreach ($all_unit_types as $type => $obj) {
    $unit_type[$type] = $type;
  }
  return $unit_type;
}

/**
 * Save the extra option for node content type.
 */
function rooms_node_node_type_save_submit($form, &$form_state) {
  if (intval($form_state['values']['days_in_index_index'])) {
    variable_set('rooms_node_days_in_index_' . $form['#node_type']->type, $form_state['values']['days_in_index_index']);
  }
  if ($form_state['values']['enable_rooms_node'] == 1 || $form_state['values']['enable_rooms_node'] == 0 || $form_state['values']['enable_rooms_node'] == "") {
    variable_set('rooms_node_' . $form['#node_type']->type, $form_state['values']['enable_rooms_node']);
  }
  if (in_array($form_state['values']['node_unit_type'], rooms_node_get_unit_types())) {
    variable_set('rooms_node_' . $form['#node_type']->type . "_unit_type", $form_state['values']['node_unit_type']);
  }
}
