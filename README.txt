CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Mantainers


INTRODUCTION
------------
Rooms Node extend the funcionalities of the Rooms module for all node types.

- Availability and prices dates for the nodes types
- An index of dates with prices and availabilities for using views exposed 
  filters
- All of the fields of a rooms_unit is cloned into the node type
- When a node is deleted also the Room unit is deleted
- When a price, availability or a customer booking a resource the index is 
  updated automatically
- You can add "Book now" button in full node page as block
- You can select another interval in full node page by block

At the moment all this functionalities working without 
any hack to the Rooms module.

This module is thinked to manage and search in thousands of bookable items 
using views, rooms module is not ready for that because 
of its DB structure.
To bypass this issue all functionalities of rooms module
are been moved into nodes for better flexibility and management.

Rooms node provide a linear index that add a row 
for each day/nid with price and state. 
It's possible to query the availabilities of rooms_unit using "sql standard" 
or views and not php code.
Rooms node expose a new date filter to views that search in a range of dates.


REQUIREMENTS
------------
This module requires the Rooms module:
 * Rooms (https://www.drupal.org/project/rooms)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Configure and install Rooms (https://www.drupal.org/project/rooms)
 * Go to the node type edit page (admin/structure/types/manage/contenttype)
 * Enable the Rooms Node for this content type
 * Select the "Room unit type" for this node
 * Select how many days you want on the index, default is 365.
 * After saving the node you can see an availability and prices tabs in 
   rooms entity and node


 The steps to perform a search with views using a date filter :
 
 - create a view for the node type you choise for manage rooms
 - add some fields to the view
 - add a new FILTER CRITERIA of Type "Rooms node" > "Rooms Date"
 - In this filter select as format "BETWEEN" and expose it 
   to let user ability to change it.
 - In the OTHER settings of the VIEW you can find 
   a setting called "Query settings" click on it and select 
   DISTINCT to avoid duplicated in view result.


TROBLESHOOTING
--------------
To avoid duplication of indexes (nid|unit_id) in multilingual installations, 
we recommend using entity_traslation and not duplicate nodes using instead 
per field translations. 
In case you want to have different room_unit by language, 
with different prices and availability you can use the content 
translation module in the core.


MAINTAINERS
-----------
Current mantainer:
 * ziomizar - https://www.drupal.org/user/814348
