<?php
/**
 * @file
 * Views integration for rooms_node.
 */

/**
 * Implements hook_views_api().
 */
function rooms_node_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Implements hook_views_data().
 */
function rooms_node_views_data() {
  $data = array();
  $data['rooms_availability_index']['table']['group'] = t('Rooms Node');
  $data['rooms_availability_index']['table']['join'] = array(
    // Directly links to node table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // Definition for other fields here.
  $data['rooms_availability_index']['date'] = array(
    'title' => t('Rooms Date'),
    'help' => t('Rooms Date'),
    'field' => array(
      'handler' => 'views_handler_field_datetime',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'rooms_node_handler_filter_datetime',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
      'empty field name' => t('Undated'),
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );

  // Definition for other fields here.
  $data['rooms_availability_index']['state'] = array(
    'title' => t('Rooms Status'),
    'help' => t('Rooms Status'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'empty field name' => t('Undated'),
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
  );

  // Definition for other fields here.
  $data['rooms_availability_index']['price'] = array(
    'title' => t('Rooms Price'),
    'help' => t('Rooms Price'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'empty field name' => t('Undated'),
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  return $data;
}
